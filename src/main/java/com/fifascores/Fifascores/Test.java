package com.fifascores.Fifascores;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.fifascores.modele.Player;
import com.fifascores.modele.Score;
import com.fifascores.modele.Team;
import com.fifascores.modele.TeamScore;

public class Test {

	public static void main(String[] args) {
		Configuration cfg = new Configuration();
		cfg.configure("hibernate.cfg.xml");// populates the data of the configuration file
		// creating session factory object
		Transaction t = null;
		Session session = null;
		try {
			SessionFactory factory = cfg.buildSessionFactory();
			// creating session object
			session = factory.openSession();
			t = session.beginTransaction();
			Score score = new Score();
			TeamScore home = new TeamScore();
			TeamScore away = new TeamScore();
			Player ja = new Player();
			Player ja2 = new Player();
			Player on1 = new Player();
			Player on2 = new Player();
			Set<Player> my = new HashSet<Player>();
			Set<Player> oni = new HashSet<Player>();
			ja.setNickname("Janek");
			ja2.setNickname("Tomek");
			on1.setNickname("Kasztan");
			on2.setNickname("Kasztanik");
			my.add(ja);
			my.add(ja2);
			oni.add(on1);
			oni.add(on2);
			Team manu = new Team("Manchester");
			session.save(manu);
			Team city = new Team("City");
			session.save(city);
			home.setResult(3);
			home.setPlayers(my);
			away.setResult(2);
			away.setPlayers(oni);
			away.setTeam(city);
			session.save(away);
			home.setTeam(manu);
			manu.getTeamScores().add(home);
			session.save(home);
			
			score.setDate(new Date());
			score.setGuestTeamScore(away);
			score.setHomeTeamScore(home);
			session.save(score);
			t.commit();
			session.flush();
			session.close();
		} catch (Exception e) {
			   e.printStackTrace(); // put a break-point here and inspect the 'e'
//			   System.out.println(e.getMessage());
		} finally {
		
		}
	}
}
