package com.fifascores.Fifascores;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FifascoresApplication {

	public static void main(String[] args) {
		SpringApplication.run(FifascoresApplication.class, args);
	}
}
