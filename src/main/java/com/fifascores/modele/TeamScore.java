package com.fifascores.modele;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name="TeamScores")
public class TeamScore {
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name="TeamWithPlayers", joinColumns = { @JoinColumn(name = "id")},  inverseJoinColumns = { @JoinColumn(name = "nickname")})
	private Set<Player> players;
	@ManyToOne(cascade = CascadeType.PERSIST)
	@JoinColumn(name="teamId")
	private Team team;
	private int result;
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	long teamScoreId;
	public long getTeamScoreId() {
		return teamScoreId;
	}

	public void setTeamScoreId(long id) {
		this.teamScoreId = id;
	}

	public Set<Player> getPlayers() {
		return players;
	}

	public void setPlayers(Set<Player> players) {
		this.players = players;
	}

	public Team getTeam() {
		return team;
	}

	public void setTeam(Team team) {
		this.team = team;
	}

	public int getResult() {
		return result;
	}

	public void setResult(int result) {
		this.result = result;
	}


}
