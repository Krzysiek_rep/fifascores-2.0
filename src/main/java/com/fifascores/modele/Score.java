package com.fifascores.modele;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
@Entity
@Table( name = "SCORES" )
public class Score {
	@Id
	@GeneratedValue(generator="increment")
	@GenericGenerator(name="increment", strategy = "increment")
	private Long id;
	private Date date;
	@OneToOne(cascade=CascadeType.ALL)
	private TeamScore guestTeamScore; 
	@OneToOne(cascade=CascadeType.ALL)
	private TeamScore homeTeamScore;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public TeamScore getGuestTeamScore() {
		return guestTeamScore;
	}

	public void setGuestTeamScore(TeamScore guestTeamScore) {
		this.guestTeamScore = guestTeamScore;
	}

	public TeamScore getHomeTeamScore() {
		return homeTeamScore;
	}

	public void setHomeTeamScore(TeamScore homeTeamScore) {
		this.homeTeamScore = homeTeamScore;
	}

}
