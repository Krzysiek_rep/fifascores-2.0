package com.fifascores.modele;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="Teams")
public class Team {
	@Id 
    @GeneratedValue
    private long teamId;
	@OneToMany(cascade=CascadeType.ALL, mappedBy="team")
	private Set<TeamScore> teamScores = new HashSet<TeamScore>(0);
	private String name;
	
	public Team() {};
	public Team(String name) {
		this.name = name;
	};
	
	public long getTeamId() {
		return teamId;
	}
	public void setTeamId(long teamId) {
		this.teamId = teamId;
	}
	public Set<TeamScore> getTeamScores() {
		return teamScores;
	}
	public void setTeamScores(Set<TeamScore> teamScores) {
		this.teamScores = teamScores;
	}
	public void addTeamScore(TeamScore teamScore) {
		this.teamScores.add(teamScore);
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

}
